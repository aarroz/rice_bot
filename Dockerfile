FROM rust:latest

WORKDIR /usr/src/rice_bot2
COPY . .
RUN apt update -yqq && apt-get install -yqq --no-install-recommends build-essential libsodium-dev libssl-dev ffmpeg youtube-dl && cargo update && cargo build --release

CMD cargo run --release